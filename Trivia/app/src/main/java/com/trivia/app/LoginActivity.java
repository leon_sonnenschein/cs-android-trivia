package com.trivia.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

import utils.database;

import utils.validation;

public class LoginActivity extends AppCompatActivity {
    database login_db = new database(LoginActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        TextView textView = (TextView)findViewById(R.id.textViewLoginToRegister);
        String text = "don't have an account?\nclick here to register!";
        SpannableString ss = new SpannableString(text);
        ClickableSpan clickable_span = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        };
        ss.setSpan(clickable_span, 0, 46, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void loginButton(View v) {
        EditText username = (EditText) findViewById(R.id.editTextLoginUsername);
        EditText password = (EditText) findViewById(R.id.editTextLoginPassword);
        boolean is_login_valid = true;

        if (!validation.checkUserName(username.getText().toString())) {
            username.setError("username must contain only numbers or letters and be 1 - 20 characters long");
            is_login_valid = false;
        }

        if (!validation.checkPassword(password.getText().toString())) {
            password.setError("password must contain only numbers or letters and be 1 - 16 characters long");
            is_login_valid = false;
        }

        if (is_login_valid) {
            if (login_db.logUserIn(username.getText().toString(), password.getText().toString())) {
                Intent menu_activity = new Intent(this, MenuActivity.class);
                SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                editor.putInt("id", login_db.getUserId(username.getText().toString()));
                editor.apply();
               // menu_activity.putExtra("user_id", login_db.getUserId(username.getText().toString()));

                startActivity(menu_activity);

                finish();
            } else {
                Toast.makeText(this, "username or password are incorrect!", Toast.LENGTH_SHORT).show();
            }

        }
    }


}