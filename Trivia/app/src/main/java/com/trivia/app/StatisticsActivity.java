package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import utils.database;

import utils.database;

public class StatisticsActivity extends AppCompatActivity {
    database statistics_db = new database(StatisticsActivity.this);

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        int user_id = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE).getInt("id", 0);

        TextView username_text = (TextView) findViewById(R.id.textViewStatsUsername);
        username_text.setText(statistics_db.getUsername(user_id));

        TextView user_id_text = (TextView) findViewById(R.id.textViewStatsUserId);
        user_id_text.setText(String.format("user_id: %d", user_id));

        TextView stats_text = (TextView) findViewById(R.id.textViewStats);
        int[] stats = statistics_db.getStatistic(user_id);
        stats_text.setText(String.format("highest score: %d\ngames played: %d\nquestions answered: %d\ncorrect answers: %d", stats[0], stats[1], stats[2], stats[3]));
    }
}
