package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import utils.notification;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent svc=new Intent(this, LoginActivity.class);
        startService(svc);

        notification.setRepeatingNotifications(this.getApplicationContext());

        Intent login_activity = new Intent(this, LoginActivity.class);
        startActivity(login_activity);

        finish();
    }
}