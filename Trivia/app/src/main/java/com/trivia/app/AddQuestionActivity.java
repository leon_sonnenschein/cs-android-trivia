package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import utils.database;

public class AddQuestionActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    private database questions_db = new database(AddQuestionActivity.this);

    private String category = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int menu_category = item.getItemId();
        if(menu_category == R.id.MenuCategoryAnimals) {
            category = "animals";
        }
        else if(menu_category == R.id.MenuCategoryGeography) {
            category = "geography";
        }
        else if(menu_category == R.id.MenuCategoryScience) {
            category = "science";
        }
        else if(menu_category == R.id.MenuCategorySports) {
            category = "sports";
        }
        else {
            return false;
        }
        return true;
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.category_menu);
        popup.show();
    }

    private void clearFields() {
        ((EditText) findViewById(R.id.editTextTextQuestionText)).setText("");;
        ((EditText) findViewById(R.id.editTextTextCorrectAnswer)).setText("");
        ((EditText) findViewById(R.id.editTextTextWrongAnswer1)).setText("");
        ((EditText) findViewById(R.id.editTextTextWrongAnswer2)).setText("");
        ((EditText) findViewById(R.id.editTextTextWrongAnswer3)).setText("");
    }

    public void clearQuestion(View v) {
        clearFields();
    }

    public void addQuestion(View v) {
        boolean verified = true;

        EditText question = (EditText) findViewById(R.id.editTextTextQuestionText);
        EditText correct_question = (EditText) findViewById(R.id.editTextTextCorrectAnswer);
        EditText wrong_answer1 = (EditText) findViewById(R.id.editTextTextWrongAnswer1);
        EditText wrong_answer2 = (EditText) findViewById(R.id.editTextTextWrongAnswer2);
        EditText wrong_answer3 = (EditText) findViewById(R.id.editTextTextWrongAnswer3);


        if(question.getText().toString().trim().length() == 0) {
            question.setError("field is empty");
            verified = false;
        }
        if(correct_question.getText().toString().trim().length() == 0) {
            correct_question.setError("field is empty");
            verified = false;
        }
        if(wrong_answer1.getText().toString().trim().length() == 0) {
            wrong_answer1.setError("field is empty");
            verified = false;
        }
        if(wrong_answer2.getText().toString().trim().length() == 0) {
            wrong_answer2.setError("field is empty");
            verified = false;
        }
        if(wrong_answer3.getText().toString().trim().length() == 0) {
            wrong_answer3.setError("field is empty");
            verified = false;
        }
        if(category.equals("")) {
            Toast.makeText(this,"pick a category", Toast.LENGTH_SHORT).show();
            verified = false;
        }

        if(verified) {
            questions_db.addQuestion(category, question.getText().toString(), correct_question.getText().toString(),
                    wrong_answer1.getText().toString(), wrong_answer2.getText().toString(), wrong_answer3.getText().toString());
            clearFields();
            Toast.makeText(this,"question registered", Toast.LENGTH_SHORT).show();
        }
    }

}