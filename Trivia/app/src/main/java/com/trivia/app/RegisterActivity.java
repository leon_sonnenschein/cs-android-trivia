package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import utils.validation;
import utils.database;

public class RegisterActivity extends AppCompatActivity {

    database register_db = new database(RegisterActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void registerButton(View v) {
        EditText username = (EditText) findViewById(R.id.editTextRegisterUsername);
        EditText password = (EditText) findViewById(R.id.editTextRegisterPassword);
        EditText confirm_password = (EditText) findViewById(R.id.editTextRegisterConfirmPassword);
        boolean is_Register_valid = true;

        if (!validation.checkUserName(username.getText().toString())) {
            username.setError("username must contain only numbers or letters and be 1 - 20 characters long");
            is_Register_valid = false;
        }

        if (!validation.checkPassword(password.getText().toString())) {
            password.setError("password must contain only numbers or letters and be 1 - 16 characters long");
            is_Register_valid = false;
        }

        if (is_Register_valid) {
            if (confirm_password.getText().toString().equals(password.getText().toString())) {
                if (register_db.getUserId(username.getText().toString()) != -1) {
                    username.setError("username already in use");
                }
                else {
                    register_db.makeUser(username.getText().toString(), password.getText().toString());
                    register_db.makeStatistics(register_db.getUserId(username.getText().toString()));
                    Toast.makeText(this, "Registered", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                confirm_password.setError("password doesn't match");
            }

        }
    }
}