package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

public class EndGameActivity extends AppCompatActivity {

    int score;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);


        fadeOutImage();

        score = getIntent().getExtras().getInt("score",-1);
        int num_of_questions = getIntent().getExtras().getInt("num_of_questions",-1);

        TextView textView = (TextView) findViewById(R.id.textViewShowResults);
        textView.setText("score: " + Integer.toString(score) + "\nquestions: " + Integer.toString(num_of_questions));
    }

    private void fadeOutImage()
    {
        ImageView trivia_logo = (ImageView) findViewById(R.id.imageViewEndGameTrivia);

        // making the animation
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(3000); // animation lasts 3 seconds

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            // hiding image when animation is over
            public void onAnimationEnd(Animation animation)
            {
                trivia_logo.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        trivia_logo.startAnimation(fadeOut); // applying the animation
    }

    public void launchMenu(View v) {
        Intent menu_activity = new Intent(this, MenuActivity.class);

        startActivity(menu_activity);

        finish();
    }

    public void launchSendMail(View v) {
        Intent send_mail_activity = new Intent(this, SendMailActivity.class);

        send_mail_activity.putExtra("score", score);
        startActivity(send_mail_activity);
    }
}