package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);

        user_id =  getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE).getInt("id", 0);  //getIntent().getIntExtra("user_id", 0);
    }

    public void launchGame(View v) {
        Intent game_activity = new Intent(this, GameActivity.class);
        startActivity(game_activity);
        finish();
    }

    public void launchStatistics(View v) {
        Intent statistic_activity = new Intent(this, StatisticsActivity.class);
        startActivity(statistic_activity);
    }

    public void launchAddQuestion(View v) {
        Intent add_question_activity = new Intent(this, AddQuestionActivity.class);
        startActivity(add_question_activity);
    }

    public void logoutButton(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int menu_play = item.getItemId();
        String category;

        if(menu_play == R.id.MenuPlayAnimals) {
            category = "animals";
        }
        else if(menu_play == R.id.MenuPlayGeography) {
            category = "geography";
        }
        else if(menu_play == R.id.MenuPlayScience) {
            category = "science";
        }
        else if(menu_play == R.id.MenuPlaySports) {
            category = "sports";
        }
        else if(menu_play == R.id.MenuPlayBasicmath) {
            category = "basicmath";
        }
        else if(menu_play == R.id.MenuPlayLetters) {
            category = "letters";
        }
        else {
            return false;
        }

        Intent game_activity = new Intent(this, GameActivity.class);
        game_activity.putExtra("category", category);
        startActivity(game_activity);
        finish();

        return true;
    }

    public void playPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.play_menu);
        popup.show();
    }
}