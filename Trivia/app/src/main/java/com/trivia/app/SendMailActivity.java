package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SendMailActivity extends AppCompatActivity {

    int score;
    String email_subject = "NEW TRIVIA SCORE!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);

        score = getIntent().getExtras().getInt("score",-1);
    }

    public void sendMail(View v) {
        String[] to_emails = (((EditText)findViewById(R.id.editTextSendMailScore)).getText().toString()).split(", *");

        Intent intent_mail = new Intent(Intent.ACTION_SENDTO);
        intent_mail.setData(Uri.parse("mailto:"));
        intent_mail.putExtra(android.content.Intent.EXTRA_EMAIL, to_emails);

        intent_mail.putExtra(Intent.EXTRA_SUBJECT, email_subject);
        intent_mail.putExtra(Intent.EXTRA_TEXT, "I got a score of: " + Integer.toString(score) + "!");

        startActivity(Intent.createChooser(intent_mail, "choose one application"));

    }
}