package com.trivia.app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import questions.CategoryQuestion;
import questions.QuestionManager;
import questions.QuestionTypeAddition;
import questions.QuestionTypeAlphabet;
import questions.QuestionTypeSubtraction;
import utils.database;

import questions.GeneralQuestion;


public class GameActivity extends AppCompatActivity {
    private QuestionManager questions_obj;
    database game_db = new database(GameActivity.this);
    private int questions_count = 1;
    private int correct_question_count = 0;
    private int NUM_OF_QUESTIONS = 9;

    GeneralQuestion.Question question;

    MusicService mService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        this.setUpcomingQuestions(getIntent().getExtras().getString("category",""));

        playAudio();

        newQuestion();
    }

    private void setUpcomingQuestions(String category) {
        if (category.equals("basicmath")) {
            this.questions_obj = new QuestionManager(new QuestionTypeAddition(), new QuestionTypeSubtraction());
        }
        else if (category.equals("letters")) {
            this.questions_obj = new QuestionManager(new QuestionTypeAlphabet());
        }
        else {
            ArrayList<ArrayList<String>> all_questions = game_db.getQuestions(category);

            if (all_questions.size() <= 0) {
                // default questions if no values were found
                this.questions_obj = new QuestionManager(new QuestionTypeAddition(), new QuestionTypeSubtraction(), new QuestionTypeAlphabet());
            }
            else {
                this.questions_obj = new QuestionManager(new CategoryQuestion(all_questions));
                NUM_OF_QUESTIONS = Math.min(all_questions.size() - 1, NUM_OF_QUESTIONS); // update number of questions base on available questions
            }
        }
    }

    @SuppressLint("SetTextI18n")
    void newQuestion() {
        question = questions_obj.getQuestion();

        ((TextView) findViewById(R.id.textViewQuestionText)).setText(question._question_text);
        ((TextView) findViewById(R.id.textViewQuestionCount)).setText(Integer.toString(questions_count));

        // setting all four possible answers
        ((Button) findViewById(R.id.buttonAnswer1)).setText(question._answers[0]);
        ((Button) findViewById(R.id.buttonAnswer2)).setText(question._answers[1]);
        ((Button) findViewById(R.id.buttonAnswer3)).setText(question._answers[2]);
        ((Button) findViewById(R.id.buttonAnswer4)).setText(question._answers[3]);
    }

    public void answerButton(View v) {
        Button button = (Button) v;
        if (button.getText() == question._correct_answer) { // answer is correct
            correct_question_count++;
        }

        if (questions_count > NUM_OF_QUESTIONS) { // game is over
            game_db.updateStatisticsAfterGame(getApplicationContext().getSharedPreferences
                    ("user", Context.MODE_PRIVATE).getInt("id", 0), NUM_OF_QUESTIONS + 1, correct_question_count);

            Intent end_game_activity = new Intent(this, EndGameActivity.class);
            end_game_activity.putExtra("score", correct_question_count);
            end_game_activity.putExtra("num_of_questions", NUM_OF_QUESTIONS + 1);

            stopAudio();

            startActivity(end_game_activity);

            finish();
        }
        else {
            questions_count++;
            newQuestion();
        }
    }

    private void playAudio() {
        Intent serviceIntent = new Intent(this, MusicService.class);

        String strLink = "https://ce-sycdn.kuwo.cn/5b3bb06f745776a5488605b857ae9f0a/62795063/resource/n3/3/69/2670300975.mp3";
        serviceIntent.putExtra("Link", strLink);
        try {
            bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void stopAudio() {
        try {
            unbindService(connection);
            mBound = false;
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MusicService.LocalBinder binder = (MusicService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

}