package com.trivia.app;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import utils.notification;

public class NotificationsBroadcastReceiver extends BroadcastReceiver {
    public static int notificationID = 1;
    public static String channelID = "channel_id";

    private NotificationManager notification_channel = null;

    /**
     * check if the app is running in the foreground
     * @return true if the app is running in foreground, false otherwise
     */
    public boolean isAppInForegrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }

    /**
     * if the app is not running in the foreground, makes and shows a notification
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isAppInForegrounded()) {

            // check if notification channel exists
            if (notification_channel == null) {
                // create notification channel
                notification_channel = notification.makeNotificationChannel(context, channelID, "channel_name", "channel_description");
            }

            android.app.Notification notification = utils.notification.makeNotification(context, intent, channelID, "Play Trivia Now!", "click here!");
            notification_channel.notify(notificationID, notification); // show the notification
        }
    }
}



