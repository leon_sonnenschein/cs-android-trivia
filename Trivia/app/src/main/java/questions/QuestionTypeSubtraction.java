package questions;

public class QuestionTypeSubtraction extends GeneralQuestion{
    int MAX_NUMBER = 20;

    public Question getQuestion() {
        int var1 = rand.nextInt(MAX_NUMBER) + 1, var2 = rand.nextInt(MAX_NUMBER) + 1; // getting two random numbers
        int answer = var1 - var2; // getting the answer of the question
        int wrong_answer1 = answer + 1, wrong_answer2 = answer - 1, wrong_answer3 = answer + rand.nextInt(3) + 2, wrong_answer4 = answer + rand.nextInt(5) + 3; // getting wrong answers
        String question_text = Integer.toString(var1) + " - " + Integer.toString(var2); // formatting question in text form
        int correct_answer_pos = rand.nextInt(MAX_POSITION);


        String[] answers = new String[MAX_POSITION];

        // putting 4 wrong answers into an array
        answers[0] = Integer.toString(wrong_answer1);
        answers[1] = Integer.toString(wrong_answer2);
        answers[2] = Integer.toString(wrong_answer3);
        answers[3] = Integer.toString(wrong_answer4);

        // replacing one of the 4 wrong answers with the right one
        answers[correct_answer_pos] = Integer.toString(answer);

        return new Question(question_text, answers, Integer.toString(answer));
    }
}
