package questions;

import java.util.ArrayList;
import java.util.Collections;

public class CategoryQuestion extends GeneralQuestion {
    private ArrayList<ArrayList<String>> all_questions;

    public CategoryQuestion(ArrayList<ArrayList<String>> all_questions) {
        this.all_questions = all_questions;

        Collections.shuffle(this.all_questions);
    }

    public Question getQuestion() {
        ArrayList<String> single_question = this.all_questions.get(0);
        this.all_questions.remove(0);

        String question = single_question.get(0);
        String correct_answer = single_question.get(1);

        single_question.remove(0);

        Collections.shuffle(single_question);

        return new Question(question,
                new String[]{single_question.get(0), single_question.get(1), single_question.get(2), single_question.get(3)}, correct_answer);
    }
}
