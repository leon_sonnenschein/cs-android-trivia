package questions;

import java.util.Random;

public abstract class GeneralQuestion {
    public final Random rand = new Random(); // used to make questions at random
    public final static int MAX_POSITION = 4;

    public abstract Question getQuestion();

    public static class Question {
        /**
         * format for storing questions
         */
        public String _question_text;
        public String[] _answers;
        public String _correct_answer;

        public Question(String question_text, String[] answers, String correct_answer) {
            _question_text = question_text;
            _answers = answers;
            _correct_answer = correct_answer;
        }
    }
}
