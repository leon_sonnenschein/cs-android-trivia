package questions;

import java.util.*;

public class QuestionTypeAlphabet extends GeneralQuestion{
    /** question format is "what letter is between [letter1] _ [letter2]*/

    private final String[] alphabet_upper_list = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private ArrayList<Integer> range_of_integers = new ArrayList<Integer>(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23));

    private final static int range_of_letters = 23; // 0 - 23 : A - X. minimum letter is X (what letter is between X _ Z)

    /** generate four solutions (3 incorrect and one correct) */
    private String[] makeAnswers(String letter_first, String letter_second, String letter_answer, int correct_answer_pos) {
        String[] answers = new String[MAX_POSITION];

        for (int j=0, i = 0; i < MAX_POSITION; i++, j++) {
            if (i == correct_answer_pos) {
                answers[i] = letter_answer;
                j--;
            }
            // checking if the chosen letter is the same as the: answer / first letter / second letter
            else if (!alphabet_upper_list[range_of_integers.get(j)].equals(letter_first) && !alphabet_upper_list[range_of_integers.get(j)].equals(letter_first) &&
                    !alphabet_upper_list[range_of_integers.get(j)].equals(letter_second)) {
                answers[i] = alphabet_upper_list[range_of_integers.get(j)];
            }
            else {
                i--; // returning one index if it hasn't been used
            }
        }

        return answers;
    }

    public Question getQuestion() {
        int letter_pos = rand.nextInt(range_of_letters), correct_answer_pos = rand.nextInt(MAX_POSITION);
        String letter_first = alphabet_upper_list[letter_pos], letter_second = alphabet_upper_list[letter_pos + 2], letter_answer = alphabet_upper_list[letter_pos + 1];

        String question_text = "what letter is between " + letter_first + " _ " + letter_second;

        Collections.shuffle(range_of_integers, new Random());

        return new Question(question_text, makeAnswers(letter_first, letter_second, letter_answer, correct_answer_pos), letter_answer);
    }
}
