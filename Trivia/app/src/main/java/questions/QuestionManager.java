package questions;

import java.util.Random;

public class QuestionManager {
    // array of objects that create question, so a wide variety of questions can be created without taking any memory
    private GeneralQuestion[] questions;
    private final Random rand = new Random();

    /**
     * makes an array of objects that create questions
     * @param args any number of objects that create questions
     */
    public QuestionManager(GeneralQuestion... args) {
        this.questions = new GeneralQuestion[args.length];

        int i = 0;
        for (GeneralQuestion arg : args) {
            this.questions[i++] = arg;
        }
    }

    /**
     * @return a random question with a random subject
     */
    public GeneralQuestion.Question getQuestion() {
        return questions[rand.nextInt(questions.length)].getQuestion();
    }
}
