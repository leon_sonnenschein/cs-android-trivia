package utils;

import java.util.regex.Pattern;

public class validation {
    static String username_regex = "^(?=.{1,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"; // only numbers or letters, 1 - 20 characters
    static String password_regex = "^(?=.*[A-Za-z])[A-Za-z\\d]{1,16}$"; // only numbers or letters, 1 - 16 characters

    public static boolean checkUserName(String username) {
        return Pattern.matches(username_regex, username);
    }

    public static boolean checkPassword(String password) {
        return Pattern.matches(password_regex, password);
    }
}
