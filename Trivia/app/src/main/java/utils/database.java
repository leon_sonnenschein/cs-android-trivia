package utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class database extends SQLiteOpenHelper {
    public Context context;
    public static final String DATABASE_NAME = "TriviaGame.db";
    public static final int DATABASE_VERSION = 1;

    private static final String COLUMN_ID = "id";

    private static final String USERS_TABLE = "users";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";

    private static final String STATISTICS_TABLE = "statistics";
    private static final String COLUMN_HIGHEST_SCORE = "highest_score";
    private static final String COLUMN_GAMES_PLAYED = "games_played";
    private static final String COLUMN_QUESTION_ANSWERED = "questions_answered";
    private static final String COLUMN_QUESTION_ANSWERED_CORRECTLY = "questions_answered_correctly";

    private static final String QUESTIONS_TABLE = "questions";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_QUESTION = "question";
    private static final String COLUMN_CORRECT_ANSWER = "correct_answer";
    private static final String COLUMN_WRONG_ANSWER1 = "wrong_answer1";
    private static final String COLUMN_WRONG_ANSWER2 = "wrong_answer2";
    private static final String COLUMN_WRONG_ANSWER3 = "wrong_answer3";



    public database(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        String users_table = "CREATE TABLE " + USERS_TABLE  + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_USERNAME + " TEXT, " +
                COLUMN_PASSWORD + " TEXT);";
        String statistics_table = "CREATE TABLE " + STATISTICS_TABLE  + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_HIGHEST_SCORE + " INTEGER, " +
                COLUMN_GAMES_PLAYED + " INTEGER, " +
                COLUMN_QUESTION_ANSWERED + " INTEGER, " +
                COLUMN_QUESTION_ANSWERED_CORRECTLY + " INTEGER);";
        String questions_table = "CREATE TABLE " + QUESTIONS_TABLE  + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CATEGORY + " TEXT, " +
                COLUMN_QUESTION + " TEXT, " +
                COLUMN_CORRECT_ANSWER + " TEXT, " +
                COLUMN_WRONG_ANSWER1 + " TEXT, " +
                COLUMN_WRONG_ANSWER2 + " TEXT, " +
                COLUMN_WRONG_ANSWER3 + " TEXT);";

        db.execSQL(questions_table);
        db.execSQL(statistics_table);
        db.execSQL(users_table);
        Log.d("DBUG", "DB was created");
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + USERS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + STATISTICS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + QUESTIONS_TABLE);
        onCreate(db);
    }

    public void updateStatisticsAfterGame(int user_id, int questions_answered, int questions_answered_correctly) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "UPDATE statistics SET " +
                "games_played = games_played + 1, " +
                "questions_answered = questions_answered + " + Integer.toString(questions_answered) + ", " +
                "questions_answered_correctly = questions_answered_correctly + " + Integer.toString(questions_answered_correctly) + ", " +
                "highest_score = CASE WHEN highest_score<" + Integer.toString(questions_answered_correctly) +
                " THEN " + Integer.toString(questions_answered_correctly) + " ELSE highest_score END " +
                "WHERE id = " + Integer.toString(user_id);

        db.execSQL(query);
    }

    /**
     * finds an id based on the username
     * @param username username of the user
     * @return id if found, -1 if not
     */
    public int getUserId(String username) {
        SQLiteDatabase db = this.getReadableDatabase();

        int user_id = -1;
        Cursor cursor = db.rawQuery("select id from users WHERE username = '" + username + "'", null);

        if (cursor.moveToFirst() && cursor.getCount() >= 1) {
            user_id = cursor.getInt(0);
        }

        cursor.close();

        return user_id;
    }

    /**
     * finds a username base on id
     * @param id id of the user
     * @return username if found, "Error" otherwise
     */
    public String getUsername(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String username = "Error";

        Cursor cursor = db.rawQuery("select username from users WHERE id = " + id, null);

        if (cursor.moveToFirst() && cursor.getCount() >= 1) {
            username = cursor.getString(0);
        }

        cursor.close();

        return username;
    }

    /**
     * checks to see if there is a user in the database with corresponding username and password
     * @param username user username
     * @param password user password
     * @return true if a user is found, false otherwise
     */
    public boolean logUserIn(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select username from users WHERE username == '" + username + "' and password == '" + password + "'", null);

        if (cursor.getCount() == 0) {
            return false;
        }

        cursor.close();

        return true;
    }

    /**
     * gets user statistics based on user id
     * @param user_id user id
     * @return statistics of the user if exists, empty array otherwise
     */
    public int[] getStatistic(int user_id) { //select highest_score, games_played, questions_answered, questions_answered_correctly from statistics where id = 2
        SQLiteDatabase db = this.getReadableDatabase();
        int[] stats = new int[5];

        Cursor cursor = db.rawQuery("select highest_score, games_played, questions_answered, questions_answered_correctly from statistics where id = " + user_id, null);

        if (cursor.getCount() >= 1) {
            cursor.moveToNext();

            // getting the data from the different columns
            stats[0] = cursor.getInt(0);
            stats[1] = cursor.getInt(1);
            stats[2] = cursor.getInt(2);
            stats[3] = cursor.getInt(3);
        }

        cursor.close();

        return stats;
    }

    /**
     * creating a user
     * @param username unique username
     * @param password user password
     */
    public void makeUser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_USERNAME, username);
        cv.put(COLUMN_PASSWORD, password);
        db.insert(USERS_TABLE, null, cv);
    }

    public void addQuestion(String category, String question, String correct_answer, String wrong_answer1, String wrong_answer2, String wrong_answer3) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        
        cv.put(COLUMN_CATEGORY, category);
        cv.put(COLUMN_QUESTION, question);
        cv.put(COLUMN_CORRECT_ANSWER, correct_answer);
        cv.put(COLUMN_WRONG_ANSWER1, wrong_answer1);
        cv.put(COLUMN_WRONG_ANSWER2, wrong_answer2);
        cv.put(COLUMN_WRONG_ANSWER3, wrong_answer3);
        db.insert(QUESTIONS_TABLE, null, cv);
    }

    public ArrayList<ArrayList<String>> getQuestions(String category) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "select question, correct_answer, wrong_answer1, wrong_answer2, wrong_answer3 from questions where category = \"" + category + "\"" , null);

        ArrayList<ArrayList<String>> all_questions = new ArrayList<ArrayList<String>>();
        ArrayList<String> single_question;

        for (int i = 1; cursor.getCount() >= i; i++) {
            cursor.moveToNext();
            single_question = new ArrayList<String>();

            // getting the data from the different columns
            single_question.add(cursor.getString(0)); // question
            single_question.add(cursor.getString(1)); // correct answer
            single_question.add(cursor.getString(2)); // wrong answer 1
            single_question.add(cursor.getString(3)); // wrong answer 2
            single_question.add(cursor.getString(4)); // wrong answer 3

            all_questions.add(single_question);
        }
        cursor.close();

        return all_questions;
    }

    /**
     * makes and reset statistics row for user
     * @param user_id user id
     */
    public void makeStatistics(int user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_HIGHEST_SCORE, 0);
        cv.put(COLUMN_GAMES_PLAYED, 0);
        cv.put(COLUMN_QUESTION_ANSWERED, 0);

        cv.put(COLUMN_QUESTION_ANSWERED_CORRECTLY, 0);

        db.insert(STATISTICS_TABLE, null, cv);
    }
}
