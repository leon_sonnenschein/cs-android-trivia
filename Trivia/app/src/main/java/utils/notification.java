package utils;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.trivia.app.LoginActivity;
import com.trivia.app.MainActivity;
import com.trivia.app.NotificationsBroadcastReceiver;
import com.trivia.app.R;

import java.util.Calendar;

public class notification {
    public static NotificationManager makeNotificationChannel(Context context, String channelID, CharSequence name, String description) {
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(channelID, name,
                importance);
        channel.setDescription(description);

        NotificationManager notificationManager =
                context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        return notificationManager;
    }

    /**
     * build a notification object
     * @param channel_id channel used to send notification from
     * @param title title of the notification
     * @param content content of the notification
     * @return built notification
     */
    public static android.app.Notification makeNotification(Context context, Intent intent, String channel_id, String title, String content) {
        Intent login_intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, login_intent,
                PendingIntent.FLAG_ONE_SHOT);


       return new NotificationCompat.Builder(context, channel_id)
                .setSmallIcon(R.drawable.trivia_logo)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
    }

    /**
     * sets up a service with a repeating alarm to send a notification every N time
     * @param context current state of the application
     */
    public static void setRepeatingNotifications(Context context) {
        Calendar updateTime = Calendar.getInstance();

        updateTime.set(Calendar.SECOND, 5);

        Intent alarm_intent = new Intent(context, NotificationsBroadcastReceiver.class);
        PendingIntent pending_intent = PendingIntent.getBroadcast(
                context,
                NotificationsBroadcastReceiver.notificationID,
                alarm_intent,
                PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT
        );

        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarms.cancel(pending_intent);
        alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, updateTime.getTimeInMillis(), 1000 * 10, pending_intent);
    }
}
